package tp04_qd;

public class ShootingStage extends Stage{

	private int successfulShots;
	private final double TIME_PENALITY = 10;
	
	public ShootingStage( int successfulShots ) {
		this.successfulShots = successfulShots;
	}
	
	public int getSuccessfulShots() {
		return this.successfulShots;
	}
	
	//1(Shooting)5shots/penaltyPerMiss=10s
	
	public String toString() {
		return this.getId()+"(Shooting)"+this.getSuccessfulShots()+"shots/penaltyPerMiss="+this.TIME_PENALITY+"s";
	}
}
