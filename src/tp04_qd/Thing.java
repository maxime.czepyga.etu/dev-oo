package tp04_qd;

public class Thing {
	
	private String name;
	private int quantity;
	private Container container;
	
	public Thing(String name, int quantity, Container container) {
		this.name = name;
		this.quantity = quantity;
		this.container = container;
	}
	
	public int getQuantity() {
		return this.quantity;
	}
	
	public void setQuantity(int quantity) throws ExtendedException {
		if (quantity > this.container.getCapacity()) {
			throw new ExtendedException("La quantit� excede la caopacit� du container !");
		}
		this.quantity = quantity;
	}
	

	public boolean transferToLargerContainer() {
		boolean result;
		switch (this.container) {
		case BLISTER:
			this.container = Container.BOX;
			result = true;
			break;
		case BOX:
			this.container = Container.CRATE;
			result = true;
		default :
			result = false;
		}
		return result;
	}
	
	public  boolean add(int quantity) {
		boolean result = true;
		try {
			this.setQuantity(this.getQuantity()+quantity);
		} catch(ExtendedException e) {
			if (!this.container.equals(Container.CRATE)) {
				this.transferToLargerContainer();
				this.add(quantity);
			} else {
				System.out.println("Quantit� trop grosse !");
			}
			result = false;
		}
		return result;
	}
	
	public String toString() {
		return this.getClass()+" [name="+this.name+", quantity="+this.getQuantity()+", container="+this.container+"]";
	}
	
	public static void main(String[] args) throws ExtendedException {
		/*
		Thing thing = new Thing("Maxime", 1, Container.BLISTER);
		//thing.setQuantity(10);
		thing.add(20);
		System.out.println(thing.getQuantity());
		*/
		Thing ch1 = new Thing("uneThing", 3, Container.BOX);
		System.out.println(ch1);
		ch1.add(20);
		System.out.println(ch1 + "\n");
		Thing ch2 = new Thing("maThing", 12, Container.BOX);
		System.out.println(ch2);
		ch2.add(60);
		System.out.println(ch2 + "\n");
	}
	

}
