package tp04_qd;

import java.util.Objects;

public class Competitor {
	
	private final String NAME;
	private Country nationality;
	private Team team;
	private int id;
	private static int nbOccur = 0;
	
	public Competitor(String name, Country nationality, Team team) {
		this.NAME = name;
		this.nationality = nationality;
		this.team = team;
		this.id = Competitor.nbOccur;
		Competitor.nbOccur+=1;
	}
	
	public String getName() { return this.NAME; }
	public int getID() { return this.id; }
	public Team getTeam() { return this.team; }
	public Country getNationality() { return this.nationality; }
	
	
	public boolean isFrom(Country c) {
		return this.getNationality().equals(c);
	}
	
	public boolean isFrom(Team t) {
		return this.getTeam().equals(t);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(NAME, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Competitor other = (Competitor) obj;
		return Objects.equals(NAME, other.NAME) && id == other.id;
	}

	public String toString() {
		return this.id+"-"+this.NAME+"("+this.nationality+") -> "+this.team;
	}
	
	public static void resetCpt() {
		Competitor.nbOccur = 0;
	}

}
