package tp04_qd;

public enum Container {
	BLISTER(1), BOX(10), CRATE(50);
	
	private int capacity;
	
	Container(int capacity) {
		this.capacity = capacity;
	}
	
	int getCapacity() {
		return this.capacity;
	}
}
