package tp04_qd;

import java.util.HashMap;
import java.util.Map;

public class Stage {
	
	public static int nbOcurr;
	private int id;
	private Map<Competitor, Double> result = new HashMap<Competitor, Double>();
	
	public Stage() {
		this.id = Stage.nbOcurr;
		Stage.nbOcurr+=1;
	}
	
	public int getId() {
		return this.id;
	}
	
	public boolean idRegistered(Competitor c) throws Exception_UnknownCompetitor {
		if (!this.result.containsKey(c)) {
			throw new Exception_UnknownCompetitor("Competitor not registered.");
		} else {
			return true;
		}
	}

	public double haveResult(Competitor c) throws Exception_NoResult {
		if (this.result.get(c).equals(null)) {
			throw new Exception_NoResult("No value");
		} else {
			return this.result.get(c);
		}
	}
	
	
	public void clear() {
		this.result.clear();
	}
	
	
	public int getNbRecord() throws Exception_UnknownCompetitor {
		int result = 0;
		for (Map.Entry<Competitor, Double> mapElement : this.result.entrySet()) {
			if (this.idRegistered(mapElement.getKey())) {
				result++;
			}
		}
		return result;
	}
	
	public void resetCpt() {
		Stage.nbOcurr = 0;
	}
}
