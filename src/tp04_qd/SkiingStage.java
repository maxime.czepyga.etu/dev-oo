package tp04_qd;

public class SkiingStage extends Stage{
	
	private double distance;
	
	public SkiingStage(int distance) {
		this.distance = distance;
	}
	
	public double getDistance() {
		return this.distance;
	}
	
	//0(Skiing)1000m
	
	public String toString() {
		return super.getId() + "(Skiing)" + this.getDistance(); 
	}

}
