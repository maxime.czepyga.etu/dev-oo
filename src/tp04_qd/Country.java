package tp04_qd;

public enum Country {

	FRANCE("FR"), GERMANY("GE"), RUSSIA("RS"), SWEDEN("SE"), AUSTRIA("AT"), ITALY("IT");
	
	private String name;
	
	Country(String name){
		this.name = name;
	}
	
	public String toString(){
		return this.name;
	}
}
