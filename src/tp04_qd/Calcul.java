package tp04_qd;

import tpQU.tp04.LocalKeyboard;

public class Calcul {
	
	public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};
	
	public static int division(int index, int divisor) {
		int result = 0;
		try {
			result = tab[index]/divisor;
		} catch(ArithmeticException e) {
			System.out.println("Division par 0!");
		}
		return result;
	}
	
	public static void statement() {
		int x, y;
		x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
		y = LocalKeyboard.readInt("Write the divisor: ");
		int result = 0;
		try {
			result = division(x,y);
		} catch ( ArithmeticException e) {
			System.out.println("Exception arithmétique.");
		}
		System.out.println("The result is: " + division(x,y));
	}

}
