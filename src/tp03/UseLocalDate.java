package tp03;
import java.time.LocalDate;
import tpOO.util.Keyboard;

public class UseLocalDate {

	
	static String dateOfTheDay() {
		LocalDate date = LocalDate.now();
		return ""+date;
	}
	
	static LocalDate inputDate() {
		int year = Keyboard.readInt();
		int month = Keyboard.readInt();
		int day = Keyboard.readInt();
		LocalDate date = LocalDate.of(year,month,day);
		return date;
	}
	
	static String diffDate() {
		LocalDate d1 = inputDate();
		LocalDate d2 = inputDate();
		int year = d1.getYear()-d2.getYear();
		int months = d1.getMonthValue()-d2.getMonthValue();
		int day = d1.getDayOfMonth()-d2.getDayOfMonth();
		return ""+d1+" ,"+d2+" : "+(year*365+months*30+day);
	}
	
	static int diffDate(LocalDate d1, LocalDate d2) {
		int year = d1.getYear()-d2.getYear();
		int months = d1.getMonthValue()-d2.getMonthValue();
		int day = d1.getDayOfMonth()-d2.getDayOfMonth();
		return (year*365+months*30+day);
	}
	
	
	
}
