package tp03;
import java.time.LocalDate;

public class Task {
	
	private String idTask;
	private LocalDate creationDate;
	private LocalDate deadLine;
	private TaskStatus state;
	private String description;
	private int duration;
	
	public Task(String description, int duration) {
		this.description = description;
		this.creationDate = LocalDate.now();
		this.deadLine = LocalDate.now(); //rajouter 10 jours � la date now
		this.duration = duration;
		this.state = TaskStatus.TODO;
		this.idTask = "" + creationDate.getYear() + creationDate.getMonthValue() + 1; //num�ro auto
	}
	
	public Task(String description, LocalDate creation, LocalDate deadline, int duration){
		this.description = description;
		this.creationDate = creation;
		this.deadLine = deadline;
		this.duration = duration;
		this.state = TaskStatus.TODO;
		this.idTask = "" + creationDate.getYear() + creationDate.getMonthValue() + 1; //num�ro auto
	}
	
	public String toString() {
		return "(" + this.idTask + "=" + this.description + ":" + this.state + "(num�ro auto):" + this.deadLine;
	}
	
	public void changeStatus() {
		this.state = TaskStatus.values()[(this.state.ordinal()+1)%(TaskStatus.values().length)];
	}
	
	void changeStatus(TaskStatus st) {
		this.state = st;
	}
	
	void changeStatus(char c) {
		if (c=='t' || c=='T') { this.state = TaskStatus.TODO;}
		if (c=='o' || c=='O') { this.state = TaskStatus.ONGOING;}
		if (c=='d' || c=='D') { this.state = TaskStatus.DELAYED;}
		if (c=='f' || c=='F') { this.state = TaskStatus.FINISHED;}
	}
	
	boolean isLate() {
		return (UseLocalDate.diffDate(this.deadLine, this.creationDate)<0);
	}
	
	void delay(int nbDays) {
		int year = this.creationDate.getYear();
		int months = this.creationDate.getMonthValue();
		int day = this.creationDate.getDayOfMonth()+nbDays;
		this.creationDate = LocalDate.of(year,months,day);
	}
	
	/*
	Person pers = new Person("NoName");
	Order o1 = new Order(pers);
	Order o2 = new Order(pers);
	Order o3 = new Order(pers);
	System.out.println("o1 -> " + o1.getId());
	System.out.println("o2 -> " + o2.getId());
	System.out.println("o3 -> " + o3.getId());
	*/
}
