package tp03;

public class UseCard {
	
	static public void main(String[] args) {
		Card card1 = new Card(Color.HEART, Rank.TEN);
		Card card2 = new Card(Color.SPADE, Rank.TEN);
		System.out.println(""+card1.getColor()+card1.getRank());
		System.out.println(""+card2.getColor()+card2.getRank());
		System.out.println("Compare rank :"+card1.compareRank(card2));
		System.out.println("Is before :"+card1.isBefore(card2));
		System.out.println("Equals :"+card1.equals(card2));
		System.out.println(card1.toString());
		System.out.println(card2.toString());
	}
}
