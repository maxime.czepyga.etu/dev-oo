package tp03;

public class Card {
	
	private Color color;
	private Rank rank;
	
	public Card(Color c, Rank r) {
		this.color = c;
		this.rank = r;
	}
	
	public Card(String c, String r) {
		this.color = Color.valueOf(c);
		this.rank = Rank.valueOf(r);
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public Rank getRank() {
		return this.rank;
	}
	
	public int compareRank(Card card) {
		return this.rank.ordinal()-card.rank.ordinal();
	}
	
	public int compareColor(Card card) {
		return this.color.ordinal()-card.color.ordinal();
	}
	
	public boolean isBefore(Card card) {
		if(this.rank.ordinal()<card.rank.ordinal()) { return true; }
		else if (this.rank.ordinal()==card.rank.ordinal() && this.color.ordinal()<card.color.ordinal()) { return true; }
		else { return false;}
	}
	
	public boolean equals(Card card) {
		return this.rank.ordinal()==card.rank.ordinal() && this.color.ordinal()==card.color.ordinal();
	}
	
	public String toString() {
		return "The "+this.rank+" of "+this.color+".";
	}
}