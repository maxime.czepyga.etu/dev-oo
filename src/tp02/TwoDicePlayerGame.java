package tp02;

public class TwoDicePlayerGame {
	
	DicePlayer p1;
	DicePlayer p2;
	
	TwoDicePlayerGame(DicePlayer player1, DicePlayer player2) {
		p1 = player1;
		p2 = player2;
	}
	
	DicePlayer winner() {
		if (p2.nbDiceRolls>p1.nbDiceRolls) return p1;
		if (p2.nbDiceRolls==p1.nbDiceRolls) {
			if (p2.totalValue>p1.totalValue) {
				return p2;
			} else {
				return p1;
			}
		}
		return p2;
	}
	
	public static void main(String[] args) {
		TwoDicePlayerGame tdpg = new TwoDicePlayerGame(new DicePlayer("Maxime"), new DicePlayer("Victor"));
		System.out.println(tdpg.winner());
	}
}
