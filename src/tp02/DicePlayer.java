package tp02;

public class DicePlayer {

	String name;
	int totalValue;
	int nbDiceRolls;
	
	DicePlayer(String name){
		this.name = name;
	}
	
	void play(Dice dice6) {
		this.totalValue+=dice6.value;
	}
	
	public String toString() {
		return ""+this.name+": "+this.totalValue+" points en "+this.nbDiceRolls+" coups.";
	}
	
	
}
