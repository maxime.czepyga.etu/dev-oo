package tp02;

public class UseCompetitor {
	
	public static void main(String[] args) {
		Competitor c1 = new Competitor(1,45,15,20);
		Competitor c2 = new Competitor(2,32,12,45);
		Competitor c3 = new Competitor(5,12,13,59);
		Competitor c4 = new Competitor(12,12,15,70);
		Competitor c5 = new Competitor(32,75,15,20);
		System.out.println(c1.display()+"\n"+c2.display()+"\n"+c3.display()+"\n"+c4.display()+"\n"+c5.display());
	}
}
