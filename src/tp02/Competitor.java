package tp02;

public class Competitor {

	String numberSign;
	int time;
	int score;
	
	Competitor(int numberSign, int score, int min, int sec){
		if (numberSign<=100 && numberSign>=1) this.numberSign = "No"+numberSign;
		if (score<=50 && score>=0) this.score = score;
		if (min<=60 && min>=0 && sec<=60 && sec>= 0) this.time = min*60+sec;
	}
	
	String display() {
		return "[" + this.numberSign+", "+this.score+" points, "+this.time+" s" + "]";
	}
}
