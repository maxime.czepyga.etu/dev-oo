package tp02;

public class OneDicePlayerGame {

	public static void main(String[] args) {
		Dice dice = new Dice(6);
		DicePlayer player = new DicePlayer("Maxime");
		while (player.totalValue<20) {
			dice.roll();
			player.play(dice);
			player.nbDiceRolls += 1;
		}
		System.out.println(player.toString());
	}
}
