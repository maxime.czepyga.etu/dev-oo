package tp02;
import java.lang.Math;

public class Dice {
	
	int numberSides;
	double rand=Math.random();
	int value;
	
	Dice(int nb){
		if (nb>1) {
			this.numberSides = nb;
		} else { 
			this.numberSides = 1; 
		}
	}
	
	void roll() {
		this.value = (int)(rand*this.numberSides);
	}
	
	public String toString() { return ""+this.value; }
}
