package tp02;

public class DiceGame {

	public static void main(String[] args) {
		Dice dice = new Dice(6);
		TwoDicePlayerGame game = new TwoDicePlayerGame("Maxime", "Victor");
		while(game.p1.totalValue<20) {
			dice.roll();
			game.p1.play(dice);
			game.p1.nbDiceRolls += 1;
		}
		dice = new Dice(6);
		while(game.p2.totalValue<20) {
			dice.roll();
			game.p2.play(dice);
			game.p2.nbDiceRolls += 1;
		}
		System.out.println(game.p1.toString());
		System.out.println(game.p2.toString());
		System.out.println(game.winner().name + " gagne la partie !");
	}
}
