package tp09;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

public class TestSerialisation {
	
	public static String path = "C:\\mes_dossiers\\travail\\but_s2\\programmation_objet\\tp\\tp09\\data.xml";
	
	static void savingAnonymous(Anonymous johnDoe, String location) {
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(location)))) {
			oos.writeObject(johnDoe);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	static void loadingAnonymous(String location) {
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(location)))) {
			Anonymous tmp = (Anonymous)ois.readObject();
			System.out.println(tmp);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Anonymous ano = new Anonymous("Maxime", LocalDate.of(2019, 4, 1));
		System.out.println(ano);
		TestSerialisation.savingAnonymous(ano, TestSerialisation.path);
		TestSerialisation.loadingAnonymous(TestSerialisation.path);
	}

}
