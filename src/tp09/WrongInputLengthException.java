package tp09;

public class WrongInputLengthException extends Exception{
	
	public WrongInputLengthException() {super();}
	public WrongInputLengthException(String message) {super(message);}

}
