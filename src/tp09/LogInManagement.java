package tp09;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LogInManagement {
	
	public static final Map<String, String> USER_INFORMATIONS  = new HashMap<String, String>();
	
	public LogInManagement() {
		LogInManagement.USER_INFORMATIONS.put("maxime.czepyga.etu", "123");
		LogInManagement.USER_INFORMATIONS.put("houzayfa.ghadri.etu", "456");
	}
	
	public void getUserPwd() throws WrongLoginException, WrongPwdException, WrongInputLengthException{
		String i = "";
		String j = "";
		/*
		do {
			System.out.println("Le login : ");
			Scanner sc = new Scanner(System.in);
			i = sc.next();
			System.out.println("Le mot de passe :");
			j = sc.next();
			if (!LogInManagement.USER_INFORMATIONS.containsKey(i)) { 
				throw new WrongLoginException("L'utilisateur n'est pas dans la base.");
			}
			if (!LogInManagement.USER_INFORMATIONS.containsValue(j)) {
				throw new WrongPwdException("Mauvais mot de passe.");
			}
		} while (!LogInManagement.USER_INFORMATIONS.get(i).equals(j));
		*/
		//else if (LogInManagement.USER_INFORMATIONS.get(i).equals(j)) return true;
		System.out.println("Le login : ");
		Scanner sc = new Scanner(System.in);
		i = sc.next();
		System.out.println("Le mot de passe :");
		j = sc.next();
		if (i.length() >= 25 || j.length() >= 10) {
			throw new WrongInputLengthException("Login ou mot de passe trop long.");
		}
		if (!LogInManagement.USER_INFORMATIONS.containsKey(i)) { 
			throw new WrongLoginException("L'utilisateur n'est pas dans la base.");
		}
		if (!LogInManagement.USER_INFORMATIONS.containsValue(j)) {
			throw new WrongPwdException("Mauvais mot de passe.");
		}
	}
	
	public static void main(String[] args) throws WrongLoginException, WrongPwdException, WrongInputLengthException {
		LogInManagement lim  = new LogInManagement();
		lim.getUserPwd();
	}

}
