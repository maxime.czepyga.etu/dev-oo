package tp09;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Anonymous implements Serializable{
	
	private String pseudo;
	private LocalDate registration;
	
	
	public Anonymous(String pseudo, LocalDate registration) {
		this.pseudo = pseudo;
		this.registration = registration;
	}
	
	public String getPseudo() {
		return this.pseudo;
	}
	
	public LocalDate getRegistrtation() {
		return this.registration;
	}
	
	public long getSeniority() {
		return -1*LocalDate.now().until(this.registration, ChronoUnit.MONTHS);
	}
	
	//Anonymous [<pseudo>,<seniority>]
	
	public String toString() {
		return "[<"+this.getPseudo()+">,<"+this.getSeniority()+">]";
	}

}
