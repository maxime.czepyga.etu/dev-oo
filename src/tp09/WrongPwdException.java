package tp09;

public class WrongPwdException extends Exception{

	public WrongPwdException() {super();}
	public WrongPwdException(String message) {super(message);}
	
}
