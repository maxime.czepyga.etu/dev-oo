package tp09;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.Scanner;

public class DicoJava {
	
	public String myPath;
	public String sourceFile;
	public char separator;
	public String chaine = "";
	
	public DicoJava() {
		this.myPath = "C:\\mes_dossiers\\travail\\but_s2\\programmation_objet\\tp\\tp09-ressources-20220510\\";
		this.sourceFile = "DicoJava.txt";
	}
	
	
	public void readDicoFile(String myPath, String sourceFile) throws IOException {
		Scanner scanLine = new Scanner(new File(this.myPath+this.sourceFile));
		File mot = new File(this.myPath+"motJava.txt");
		if (mot.createNewFile()) {
			System.out.println("File create :"+mot.getName());
		} else {
			System.out.println("File already exist");
		}
		
		PrintWriter writter = new PrintWriter(mot);
		while(scanLine.hasNextLine()) {
			String line = scanLine.nextLine();
			
			String firstWord ="";
			if (line.contains("Mot-clé")) {
				firstWord = line.substring(0, line.indexOf('\t'));
				writter.println(firstWord);
			}
			writter.write(firstWord);
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		DicoJava dj = new DicoJava();
		dj.readDicoFile(dj.myPath, dj.sourceFile);
	}

}
