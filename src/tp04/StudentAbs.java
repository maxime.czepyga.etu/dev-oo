package tp04;

public class StudentAbs {
	
	private Student etu;
	private int nbAbscence;
	
	public StudentAbs(Student etu, int nbAbscence) {
		this.etu = etu;
		this.nbAbscence = nbAbscence;
	}
	
	//Q10
	
	public String toString() {
		return "L'�tudiant "+this.etu+", nombre d'abscents "+this.nbAbscence+".";
	}
	
	public boolean equals(StudentAbs other) {
		return this.equals(other);
	}
	
	public boolean warning(int thresholdAbs, double thresholdAvg) {
		return this.nbAbscence>=thresholdAbs || this.etu.getAverage() <=thresholdAvg;
	}
	
	
}
