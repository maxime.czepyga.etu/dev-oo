package tp04;
import tpOO.tp04.PendingCase;

public class PendingCaseQueue {
	
	private PendingCase[] myQueue;
	private int capacityMax;
	private int head;
	private int tail;
	
	public PendingCaseQueue(int n) {
		this.capacityMax = n;
		this.myQueue = new PendingCase[this.capacityMax];
	}
	
	public void clear() {
		int i=0;
		while(this.myQueue[i]!=null) {
			this.myQueue[i] = null;
			i++;
		}
	}
	
	public PendingCase getPendingCase(int pos) {
		return this.myQueue[pos];
	}
	
	public boolean isEmpty() {
		return this.myQueue[0]==null;
	}
	
	public boolean isFull() {
		return this.myQueue[this.capacityMax-1]==null;
	}
	
	boolean addOne(PendingCase anotherPendingCase) {
		int i=0;
		if (this.isFull()==true)return false;
		do{
			if ( this.myQueue[i] == null ) {
				this.myQueue[i] = anotherPendingCase;
			}
			i++;
		}while(this.myQueue[i-1] != null);
		return true;
	}
	
	public int indexIncrement(int idx) {
		return idx++;
	}
	
	public int size() {
		for (int i = 0; i<this.capacityMax; i++) {
			if (this.myQueue[i]==null) {
				return i;
			}
		}
		return 0;//� discuter;
	}
	
	public PendingCase removeOne() {
		return this.myQueue[this.size()-1];
	}
	
	public String toString() {
		return ""+this.myQueue+", de taille "+this.capacityMax+", de t�te "+this.head+" et de queue "+this.tail+".";
	}
	
	public double getTotalAmount() {
		double result = 0;
		for (int i=0; i<this.size(); i++) {
			result+=this.myQueue[i].getAmount();
		}
		return result;
	}
	
	public void cheating(PendingCase anotherPendingCase, int position) {
		//� faire
	}

}
