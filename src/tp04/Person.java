package tp04;

public class Person {
	
	private String id;
	private String name;
	private String forname;
	
	public Person(String id, String name, String forname) {
		this.id = id;
		this.name = name;
		this.forname = forname;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getForename() {
		return this.forname;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setForname(String forname) {
		this.forname = forname;
	}
	
	public boolean equals(Person personne) {
		return this.equals(personne);
	}
	
	public String toString() {
		return "L'identifiant "+this.getId()+", le nom "+this.getName()+" et le nom"+this.getForename()+".";
	}
}