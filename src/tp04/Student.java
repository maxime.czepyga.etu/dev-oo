package tp04;

public class Student {

	private double[] grades;
	private Person person;
	
	public Student(Person pers, double[] grades){
		this.grades = grades;
		this.person = pers;
	}
	
	private Student(String id, String name, String forename, double[] grades){
		this.person = new Person(id,name,forename);
		this.grades = grades;
	}
	
	public String getId() {
		return this.person.getId();
	}
	
	public String getName() { 
		return this.person.getName();
	}
	
	public String getForename() {
		return this.person.getForename();
	}
	
	public void setGardes(double[] grade) {
		this.grades = grade;
	}
	
	public String toString() {
		return ""+this.person.toString()+", notes"+this.grades+".";
	}
	
	public boolean equals(Student other) {
		return this.equals(other);
	}
	
	public double getAverage() {
		double result = 0;
		for (int i=0; i<this.grades.length; i++) {
			result += this.grades[i];
		}
		return result/this.grades.length;
	}
	
	public void addGrade(double aGrade) {
		double[] nGrades = new double[this.grades.length+1];
		for (int i=0; i<this.grades.length; i++) { this.grades[i] = nGrades[i];}
		nGrades[this.grades.length] = aGrade;
		this.setGardes(nGrades);
	}
}
