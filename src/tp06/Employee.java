package tp06;
import java.time.LocalDate;

public abstract class Employee {

	private String name;
	private LocalDate hringDate;
	
	public Employee(String name, LocalDate date) {
		this.name = name;
		this.hringDate = date;
	}
	
	public String getName() { return this.name; }
	public String getTitle() { return this.getClass().getSimpleName(); }
	public abstract double getWages();
	public LocalDate getHringDate() { return this.hringDate; }
	
	public String toString() { return "Name :"+this.getName()+"\nTitle :"+this.getTitle()+"\nWadges"; }
	
	public boolean isWorker() { return false; }
	public boolean isSalesperson() { return false; }
	
	public abstract boolean objectiveFulfilled(); 
}
