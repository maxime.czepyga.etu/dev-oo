package tp06;
import java.time.LocalDate;

public abstract class Salesperson extends Employee{
	
	private double turnover;
	private static double objective = 10000.0;
	
	public Salesperson(String name, LocalDate date, double turnover) {
		super(name, date);
		this.turnover = turnover;
	}
	
	public double getTurnover() { return this.turnover; }
	
	public String toString() { return "Turnover :"+this.turnover; }
	
	public boolean isSalesperson() { return true; }
	
	public boolean objectiveFulfilled() { return this.getTurnover()>=this.objective; }
}
