package tp06;
import java.util.ArrayList;
import java.time.LocalDate;

public class Company {
	
	private ArrayList<Employee> company = new ArrayList<Employee>();
		
	public void addEmployee(Employee employee) { this.company.add(employee); }
	public void supprEmployee(int index) { this.company.remove(index); }
	public void supprEmployee(Employee e) { this.company.remove(e); }
		
	public String toString() { return this.company.toString(); }
	
	public int getNbEmployee() { return this.company.size(); }
	public int getNbSalesPerson() {
		int n = 0;
		for (int i=0; i<this.company.size(); i++) {
			if (this.company.get(i).isSalesperson()) n++;
		}
		return n;
	}
	public int getNbWorker() {
		int n = 0;
		for (int i=0; i<this.company.size(); i++) {
			if (this.company.get(i).isWorker()) n++;
		}
		return n;
	}
	
	public void firing(LocalDate fatefulDate) {
		for (int i=0; i<this.company.size(); i++) {
			if (this.company.get(i).getHringDate().isAfter(fatefulDate)) this.supprEmployee(this.company.get(i));
		}
	}
	
	public void firing() {
		for (int i=0; i<this.company.size(); i++) {
			if (this.company.get(i).objectiveFulfilled()) this.supprEmployee(this.company.get(i));
		}
	}
}
