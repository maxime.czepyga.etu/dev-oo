package tp06;
import java.time.LocalDate;

public class Vendor extends Salesperson {
	
	private static final double PERCENTAGE = 0.2;
	private static final int BONUS = 400;
	
	public Vendor(String name, LocalDate date, double turnover) {
		super(name, date, turnover);
	}
	
	public String getTitle() { return this.getClass().getSimpleName(); }
	public double getWages() { return Vendor.PERCENTAGE * this.getTurnover() + Vendor.BONUS; }
}
