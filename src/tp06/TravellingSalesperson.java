package tp06;
import java.time.LocalDate;

public class TravellingSalesperson extends Salesperson {
	
	private static final double PERCENTAGE = 0.2;
	private static final int BONUS = 800;
	
	public TravellingSalesperson(String name, LocalDate date, double turnover) { super(name,date,turnover); }
	
	public String getTitle() { return this.getClass().getSimpleName(); }
	
	public double getWages() { 
		return this.getTurnover()*TravellingSalesperson.PERCENTAGE + TravellingSalesperson.BONUS; 
	}
}
