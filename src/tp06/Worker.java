package tp06;
import java.time.LocalDate;

public class Worker extends Employee {
	
	private static final double BY_UNIT = 5.0;
	private int units;
	private static int objective = 1000;
	
	public Worker(String name, LocalDate date, int units) {
		super(name, date);
		this.units = units;
	}
	
	public String getTitle() { return this.getClass().getSimpleName(); }
	public double getWages() { return this.units * this.BY_UNIT; }
	
	public String toString() { return this.getTitle()+"\n"+this.getWages(); }
	
	public boolean isWorker() { return true; }
	
	public boolean objectiveFulfilled() { return this.getWages() >= this.objective; }

}
