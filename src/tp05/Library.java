package tp05;
import java.util.ArrayList;
// ArrayList<String> cars = new ArrayList<String>();

public class Library{
	
	private ArrayList<Book> catalog;
	private Book[] catalogue;
	
	public Library() {
		this.catalog = new ArrayList<Book>();
		this.catalogue = new Book[50];
	}
	
	public Book getBook(String code) {
		int i = 0; 
		while(i<this.catalog.size() && !this.catalog.get(i).getCode().equals(code)) i++;
		if(i== this.catalog.size()) return null;
		return this.catalog.get(i);
	}
	
	
	/*public Book getBook(String code) {
		Book b = new Book("null", "null", "null", 0);
		for (int i=0; i<this.catalogue.length; i++) {
			if (this.catalogue[i].code.equals(code)) {
				b = catalogue[i];
			}
		}
		return b;
	}*/
	
	public boolean addBook(Book b) {
		boolean result = false;
		if(!this.catalog.contains(b)) {
			this.catalog.add(b);
			result = true;
		}
		return result;
	}
	/*public boolean addBook(Book b) {
		Book[] catalogue2 = new Book[this.catalogue.length+1];
		for (int i=0; i<this.catalogue.length; i++) {
			if (this.catalogue[i].equals(b)) return false;
			catalogue2[i] = this.catalogue[i];
		}
		catalogue2[this.catalogue.length] = b;
		this.catalogue = catalogue2;
		return true;
	}*/
	
	public boolean removeBook(String aCode) {
		Book b = this.getBook(aCode);
		return this.removeBook(b);
	}
	
	/*public boolean removeBook(String aCode) {
		for(int i=0; i<this.catalogue.length; i++) {
			if (this.catalogue[i].code.equals(aCode)) {this.catalogue[i]=null; return true;}
		}
		return false;
	}*/
	
	public boolean removeBook(Book b) {
		return this.catalog.remove(b);
	}
	/*public boolean removeBook(Book b) {
		for(int i=0; i<this.catalogue.length; i++) {
			if (this.catalogue[i].equals(b)) {this.catalogue[i]=null; return true;}
		}
		return false;
	}*/
	
	public String toString() {
		return this.catalog.toString();
		/*String result = "";
		for (int i=0; i<this.catalogue.length; i++) {
			result+=this.catalogue[i].toString()+"\n\n";
		}
		return result;*/
	}
	
	public String borrowings() {
		String result = "";
		for (int i=0; i<this.catalog.size(); i++) {
			if (this.catalog.get(i).isAvailable()) result+=catalog.get(i).getTitle();
		}
		return result;
	}
	
	public boolean borrow(String code, int borrower) {
		if (this.getBook(code).isAvailable()) { this.getBook(code).setBorrower(borrower); return true; }
		return false;
	}
	
	public boolean giveBack(String code) {
		if (this.getBook(code).getBorrower() != 0) { this.getBook(code).setBorrower(0); return true; }
		return false;
	}
}
