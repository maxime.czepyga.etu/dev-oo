package tp05;
import java.time.LocalDate;

public class ComicBook {
	
	private String code;
	private String title;
	private String author;
	private int publicationYear;
	private String illustrator;
	private LocalDate borrowingDate;

	ComicBook(String code, String title, String author, int publicationYear, String illustrator){
		this.code = code;
		this.title = title;
		this.author = author;
		this.publicationYear = publicationYear;
		this.illustrator = illustrator;
	}

	public String getCode() { return this.code; }
	public String getTitle() { return this.title; }
	public String getAuthor() { return this.author; }
	public int getPublicationYear() { return this.publicationYear; }
	public String getIllustrator() { return this.illustrator; }
	public int getDurationMax() { return 7; }
	
	public void setCode(String code) { this.code = code; }
	public void setTitle(String title) { this.title = title; }
	public void setAuthor(String author) { this.author = author; }
	public void setPublicationYear(int publicationYear) { this.publicationYear = publicationYear; }
	public void setIllustrator(String illustrator) { this.illustrator = illustrator; }
	
	@Override
	public String toString() {
		return "Book : "+this.getCode()+"\nTitle : "+this.getTitle()+"\nFrom : "+this.getAuthor()+"\nWritten on : "+this.getPublicationYear()+"\nIllustrate b : "+this.getIllustrator();
	}
}
