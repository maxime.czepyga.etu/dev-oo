package tp05;

public class UseLibrary {
	
	public static void main(String[] args) {
		Book b1 = new Book("001", "Vingt Mille Lieues sous les mers", "Jules Verne", 1869);
		Book b2 = new Book("002", "Divergent", "Veronica Roth", 2011);
		Book b3 = new Book("003", "Harry Potter et la coupe de feu", "JK Rowling", 2000);
		Library lib = new Library();
		lib.addBook(b1);
		lib.addBook(b2);
		lib.addBook(b3);
		/*
		System.out.println(lib.getBook("001"));
		System.out.println(lib.getBook("002"));
		System.out.println(lib.getBook("003"));
		*/
		String result = "";
		result += "Borrowings Liste\n"+lib.borrowings()+"\n"+lib.toString();
		System.out.println(result);
		
		ComicBook b4 = new ComicBook("004", "Gaston Lagaffe", "Andr� Franquin", 1957, "Yvan Delporte");
		ComicBook b5 = new ComicBook("005", "Lucky Luk", "Morris", 1946, "Ren� Goscinny");
		System.out.println("\n\nComicBook\n");
		System.out.println(b4.toString());
		System.out.println(b5.toString());
	}
}
