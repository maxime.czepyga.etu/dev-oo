package tp05;
import java.time.LocalDate;

public class Book {

	private final String code;
	private String title;
	private String author;
	private int publicationYear;
	private int borrower = 0;
	private LocalDate  borrowingDate;
	
	public Book(String code, String title, String author, int publicationYear) {
		this.code = code;
		this.title = title;
		this.author = author;
		this.publicationYear = publicationYear;
	}
	
	public String getTitle() { return this.title; }
	public String getAuthor() { return this.author; }
	public int getPublicationYear() { return this.publicationYear; }
	public String getCode() {return this.code;}
	public int getBorrower() { return this.borrower; }
	
	public void setTitle(String title) { this.title = title; }
	public void setAuthor(String author) { this.author = author; }
	public void setBorrower(int v) { this.borrower = v; }
	
	public String toString() {
		return "Code : "+this.code+"\nTitle : "+this.title+"\nAuthor : "+this.author+"\nPublication year : "+this.publicationYear;
	}
	
	public boolean borrow(int borrower) {
		if (this.borrower != 0) { this.setBorrower(borrower); return true;}
		return false;
	}
	
	public boolean giveBack() {
		if (this.borrower != 0) { this.setBorrower(0); return true;}
		return false;
	}
	
	public boolean isAvailable() { return this.borrower==0; }
	
}
