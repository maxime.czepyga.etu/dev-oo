package tp08;

import java.util.Objects;

public class Participant {

	private String name;
	private Participant parthner;
	
	public Participant(String name) {
		this.name = name;
	}

	public String getName() {return name;}
	public String getParthner() { return this.parthner.getName(); }
	
	public String toString() { return "[" + this.getName() + " -> " + this.getParthner() +"]";}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(parthner);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participant other = (Participant) obj;
		return Objects.equals(parthner, other.parthner);
	}

	public boolean isMatched() {
		return !this.getParthner().equals(null);
	}
	
	public boolean isMatchedWith(Participant p) {
		return this.parthner.equals(p);
	}
	
	public boolean matchWith(Participant p) {
		if (this.isMatched()) {
			this.parthner = p; 
			return true;
		}
		return false;
	}
	
	public void breakOff() {
		if (!this.isMatched()) {
			this.parthner = null;
		}
	}

	
}
