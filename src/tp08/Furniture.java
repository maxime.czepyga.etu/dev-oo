package tp08;

public class Furniture implements IProduct{

	private String label;
	private double price;
	private static int cpt=0;
	
	public Furniture(String label, double price) {
		if (label.equals(null)) {
			this.label = "refUnknown" + Furniture.cpt;
			Furniture.cpt++;
		} else {
			this.label = label;
		}
		this.price = price;
	}
	
	public String getLabel() { return this.label;}
	public double getPrice() { return this.price;}
	
	@Override
	public String toString() { return "["+this.label+"->"+this.price+"]";}
	
	public boolean isPerishable() { return false; }
}
