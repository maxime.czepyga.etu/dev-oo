package tp08;

import java.time.LocalDate;

public class Food implements IProduct,Comparable<Food>{

	private String label;
	private double price;
	private LocalDate bestBeforeDate;
	private static int cpt = 0;
	
	public Food(String label, double price, LocalDate bestBeforeDate) {
		if (label.equals(null)) {
			this.label = "refUnknown" + Food.cpt;
			Food.cpt++;
		} else {
			this.label = label;
		}
		this.price = price;
		this.bestBeforeDate = bestBeforeDate;
	}
	
	public Food(String label, double price) { this(label, price, LocalDate.now().plusDays(10));}
	
	public String getLabel() {return this.label;}
	public double getPrice() {return this.price;}
	public LocalDate getBestBeforeDate() { return this.bestBeforeDate;}
	
	public boolean isPerishable() { return true; }
	
	public String toString() { return "["+this.getLabel()+"="+this.getPrice()+" -> Before "+this.getBestBeforeDate()+"]";}
	
	public boolean isBefore(LocalDate aDate) { return this.bestBeforeDate.isBefore(aDate);}
	
	public int compareTo(Food aFood) {
		return this.getBestBeforeDate().compareTo(aFood.getBestBeforeDate());
	}
	
}
