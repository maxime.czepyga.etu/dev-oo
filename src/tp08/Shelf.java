package tp08;

import java.util.ArrayList;

public class Shelf{
	
	private boolean refrigerated;
	private int capacityMax;
	private ArrayList<IProduct> shelve;
	
	public Shelf(boolean refrigerated, int capacityMax) {
		this.refrigerated = refrigerated;
		this.capacityMax = capacityMax;
		this.shelve = new ArrayList<IProduct>();
	}
	
	public Shelf() {this(false,1); }
	
	public ArrayList<IProduct> getShelve(){return this.shelve;}
	public boolean getRefrigerated() { return this.refrigerated;}
	
	public boolean isFull() { return this.shelve.size() == this.capacityMax; }
	public boolean isEmpty() { return this.shelve.size() == 0;}
	public boolean isGerigerated() { return this.getRefrigerated(); }
	
	@Override
	public String toString() {
		return "[" + this.getRefrigerated() + " : " + this.capacityMax + " -> [" + this.getShelve() + "]";
	}
	
	public boolean add(IProduct p) {
		if (p.isPerishable() && this.refrigerated && !this.isFull()) {
			this.shelve.add(p);
			return true;
		} else if (!p.isPerishable() && !this.refrigerated && !this.isFull()) {
			this.shelve.add(p);
			return true;
		}
		return false;
	}

}
