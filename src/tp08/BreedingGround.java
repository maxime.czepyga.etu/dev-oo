package tp08;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class BreedingGround {

private HashSet<Participant> registrations = new HashSet<Participant>();
	
	public HashSet<Participant> getApplicants() { return this.registrations; }
	public boolean registration(Participant aPart) {
		boolean result = false;
		if (!this.getApplicants().contains(aPart)) {
			result = this.getApplicants().add(aPart); 
		}
		return result;
	}
	public List<Participant> loners() {
		List<Participant> loners = new ArrayList<Participant>();
		for (Participant aPart: this.registrations) {
			if (!aPart.isMatched()) {
				loners.add(aPart);
			}
		}
		return loners;
	}
	public List<Participant> lonersCleansing() {
		this.getApplicants().removeAll(this.loners());
		List<Participant> result = new ArrayList<Participant>();
		result.addAll(this.getApplicants());
		return result;
	}
	public void forcedMatching() {
		Random rdm = new Random();
		while(this.loners().size() > 1) {
			int idx1 = rdm.nextInt(this.loners().size()+1);
			int idx2 = idx1;
			while (idx2 == idx1) {
				idx2 = rdm.nextInt(this.loners().size()+1);
			}
			this.loners().get(idx1).matchWith(this.loners().get(idx2));
		}
	}
	public List<Participant> cheaters(BreedingGround anotherBreedingGround) {
		List<Participant> result = new ArrayList<Participant>();
		for (Participant aPart: anotherBreedingGround.getApplicants()) {
			if (this.getApplicants().contains(aPart)) {
				result.add(aPart);
			}
		}
		return result;
	}
	public void isolateCheater(Participant cheater) { cheater.breakOff(); }
	public void cheatersCleansing(BreedingGround anotherBreedingGround) {
		for (Participant aPart: this.cheaters(anotherBreedingGround)) {
			this.isolateCheater(aPart);
			anotherBreedingGround.isolateCheater(aPart);
		}
	}
	public boolean possibleMerging(BreedingGround anotherBreedingGround) { return this.cheaters(anotherBreedingGround).size() == 0; }
	public void merging(BreedingGround anotherBreedingGround) { this.getApplicants().addAll(anotherBreedingGround.getApplicants()); }
	void securedMerging(BreedingGround anotherBreedingGround) {
		if (!this.possibleMerging(anotherBreedingGround)) {
			this.cheatersCleansing(anotherBreedingGround);
		}
		this.merging(anotherBreedingGround);
	}
	public void clear() { this.getApplicants().clear(); }
	
}
