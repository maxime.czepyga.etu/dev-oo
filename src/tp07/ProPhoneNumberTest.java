package tp07;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProPhoneNumberTest {
	   public String s1, s2;
	    public ProPhoneNumber un, deux, trois, quatre;

	    @BeforeEach
	    void initialization() {
	        s1 = "1234";
	        s2 = "1111";
	        un = new ProPhoneNumber(s1, UniversityDepartement.IUT);
	        deux = new ProPhoneNumber(s1, UniversityDepartement.FST);
	        trois = new ProPhoneNumber(s2, UniversityDepartement.IUT);
	        quatre = new ProPhoneNumber(s1, UniversityDepartement.IUT);
	    }

	    @Test
	    void testEquals() {
	        assertTrue(un.equals(un));
	        assertFalse(un.equals(deux));
	        assertFalse(un.equals(trois));
	        assertTrue(un.equals(quatre));
	        assertFalse(deux.equals(trois));
	        assertFalse(deux.equals(quatre));
	    }

	    @Test
	    void 
() {
	        assertTrue(un.equals(s1));
	        assertFalse(un.equals(s2));
	        assertTrue(deux.equals(s1));
	        assertFalse(deux.equals(s2));
	    }

	    @Test
	    void testInternToString() {
	        String res = "31234(" + UniversityDepartement.IUT.getLabelShort() + ")";
	        assertEquals(res, un.internToString());
	        res = "21234(" + UniversityDepartement.FST.getLabelShort() + ")";
	        assertEquals(res, deux.internToString());
	        res = "31111(" + UniversityDepartement.IUT.getLabelShort() + ")";
	        assertEquals(res, trois.internToString());
	        res = "31234(" + UniversityDepartement.IUT.getLabelShort() + ")";
	        assertEquals(res, quatre.internToString());
	    }

	    @Test
	    void testExternToString() {
	        String res = "+33.3.20.03.12.34 (" + UniversityDepartement.IUT.getLabelLong() + ")";
	        assertEquals(res, un.externToString());
	        res = "+33.3.20.02.12.34 (" + UniversityDepartement.FST.getLabelLong() + ")";
	        assertEquals(res, deux.externToString());
	        res = "+33.3.20.03.11.11 (" + UniversityDepartement.IUT.getLabelLong() + ")";
	        assertEquals(res, trois.externToString());
	        res = "+33.3.20.03.12.34 (" + UniversityDepartement.IUT.getLabelLong() + ")";
	        assertEquals(res, quatre.externToString());
	    }

	    @Test
	    void testGetDepartment() {
	        assertEquals(UniversityDepartement.IUT, un.getDepartement());
	        assertEquals(UniversityDepartement.FST, deux.getDepartement());
	        assertEquals(UniversityDepartement.IUT, trois.getDepartement());
	        assertEquals(UniversityDepartement.IUT, quatre.getDepartement());
	    }
}
