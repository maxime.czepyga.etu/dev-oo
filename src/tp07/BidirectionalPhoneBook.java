package tp07;
import java.util.*;

import tdOO.td01.PhoneNumber;

public class BidirectionalPhoneBook {
	
	private Map<String, ProPhoneNumber> name2num;
	private Map<String, String> num2name;
	
	public BidirectionalPhoneBook() {
		this.name2num = new HashMap<String, ProPhoneNumber>();
		this.num2name = new HashMap<String, String>();
	}
	
	public int getNbEntries() { return this.name2num.size(); }
	
	public boolean alreadyRegistred(String s) {
		for (Map.Entry mapentry : num2name.entrySet()) {
	           if (mapentry.getKey().equals(s) || mapentry.getValue().equals(s)) return true;
	    }
		return false;
	}
	
	public boolean add(String name, UniversityDepartement dept, String fourDigits) {
		name2num.put(name, new ProPhoneNumber(fourDigits, dept));
		return true;
		//33,3,20,80,Integer.parseInt(fourDigits.substring(0,2)),Integer.parseInt(fourDigits.substring(2,4)
	}
}
