package tp07;
import java.util.Objects;

import tdOO.td01.PhoneNumber; 

public class ProPhoneNumber {

	private PhoneNumber pn;
	private UniversityDepartement dept;
	
	public ProPhoneNumber(String digits, UniversityDepartement dept) {
		this.pn = new PhoneNumber(33,3,20,dept.getDialingCode(),Integer.parseInt(digits.substring(0,2)),Integer.parseInt(digits.substring(2,4)));
	}
	
	public UniversityDepartement getDepartement() { return this.dept; }
	
	public String internToString() { return "3" + this.pn.standardFormat().substring(9,11)+this.pn.standardFormat().substring(12,14); }
	public String externToString() { return "+33."+this.pn.standardFormat().substring(1,14);}

	@Override
	public int hashCode() {
		return Objects.hash(pn);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		ProPhoneNumber other = (ProPhoneNumber) obj;
		return Objects.equals(pn, other.pn);
	}
	
	public boolean equals(String fourDigits) {
		return fourDigits.equals(this.pn.standardFormat().substring(9, 11)+this.pn.standardFormat().substring(12,14));
	}
	
	public static void main(String[] args) {
		PhoneNumber pn  = new PhoneNumber(33,3,20,96,32,01);
		System.out.println(pn.standardFormat());
		System.out.println(pn.internationalFormat());
	}
	
}
