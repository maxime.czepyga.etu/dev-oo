package tp07; 

public enum UniversityDepartement {
	
	F3S(1,"Facult� des Sciences de Sant� et du Sport"),
    FST(2,"Facult� des Sciences et Technologies"),
    IUT(3,"Institut Universitaire de Technologies"),
    FSJPS(4,"Facult� des Sciences Juridiques, Politiques et Sociales"),
    FSEST(5,"Facult� des Sciences �conomiques, Sociales et des Territoires"),
    FH(6,"Facult� des Humanit�s"),
    FLCS(7,"Facult� des Langues, Cultures et Soci�t�s"),
    FPSEF(8,"Facult� de Psychologie, Sciences de l�Education et de la Formation");

	private int diallingCode;
	private String Label;
	
	private UniversityDepartement(int diallingCode, String Label) {
		this.diallingCode = diallingCode;
		this.Label = Label;
	}
	
    public int getDialingCode() { return this.diallingCode; }
    public String getLabelShort() { return this.name(); }
    public String getLabelLong() { return this.Label; }
    
}
