package tp01_qd;
import java.time.LocalDate;

import tp03.Task;
import tp03.ToDoList;
import tpOO.*;

public class Event {

	private String label;
	private String place;
	private LocalDate start;
	private LocalDate end;
	private ToDoList task;

	public Event(String label, String place){
		this(label, place, LocalDate.now(), LocalDate.now().plusDays(1), new ToDoList());
	}

	public Event(String label, String place, LocalDate start){
		this (label, place, start, start.plusDays(1), new ToDoList());
	}

	public Event(String label, String place, LocalDate start, LocalDate end){
		this(label, place, start, end, new ToDoList());
	}

	public Event(String label, String place, LocalDate start, LocalDate end, ToDoList tasks){
		this.label = label;
		this.place = place;
		this.task = tasks;
		if (end.isBefore(start)) this.end=start;
		else this.end = end;
		this.start = start;
		this.end = end;
	}

	public String getLabel() { return this.label; }
	public String getPlace() { return this.place; }
	public LocalDate getStart() { return this.start; }
	public LocalDate getEnd() { return this.end; }
	public ToDoList getTasks() { return this.task; }

	public void setLabel(String label) { this.label = label; }
	public void setPlace(String place) { this.place = place; }
	public void setStart(LocalDate date) { this.start = date; }
	public void setEnd(LocalDate date) {this.end = date; }
	public void setTasks(ToDoList list) { this.task = list; }

	public String toString() { return ""+this.label+"-"+this.place+" : "+this.start+"-"+this.end;}

	public void addTask(Task aTask) { this.task.addTask(aTask); }
	public void removeTask(Task aTask) { this.task.removeTask(aTask); }
	public void removeTask(int idx) { this.task.removeTask(idx); }
	public int lengthTask() { return this.task.getNbTask(); }
	
	public boolean equals(Object o) { return this.equals(o); }
	
	public boolean overlap(Event evt) { return this.start.isBefore(evt.end) || this.end.isBefore(evt.end);}
}
