package tp01_qd;

import java.util.Objects;
/** Class Warrior Card, a Warrior, his strength and his agility.
 * @author CZEPYGA Maxime
 * @version 0.0.0.1
 *
 */

public class WarriorCard {

	/**
	 *Attibutes.
	 */
	private String name;
	private int strength;
	private int agility;
	
	/**
	 * Constructor
	 */
	public WarriorCard(String name, int strength, int agility) {
		this.name = name;
		this.strength = strength;
		this.agility = agility;
	}

	//click_droit/sources/generate...and equals
	/**
	 * Method.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WarriorCard other = (WarriorCard) obj;
		return Objects.equals(name, other.name);
	}
	
	/**
	 * Comparaison method.
	 */
	public int compareStrength(WarriorCard wc) {
		return this.strength - wc.strength;
	}
	
	/**
	 * Comparaison method.
	 */
	public int compareAgility(WarriorCard wc) {
		return this.agility - wc.agility;
	}
	
	public String toString() {
		return "Le guerrier "+this.name+", a une force de "+this.strength+" et une agilit� de "+this.agility;
	}
	
	//-encoding utf8 -docencoding utf8 -charset utf8
	//projet>Generate_javadoc
}
