package tp03_qd;

public class StudentBUT2 extends StudentBUT1{
	
	public static final int DEFAULT_DURATION = 30;
	private int duration;
	
	StudentBUT2(String name, String forename, int duration) {
		super(name, forename);
		this.duration = duration;
	}
	
	StudentBUT2(String name, String forename){
		super(name, forename);
		this.duration = Student2.DEFAULT_DURATION;
	}
	
	public String toString() {
		return this.getName() + " " + this.getForename() + "(" + this.duration + ")";
	}
}
