package tp03_qd;

public class Person {
	
	private String name;
	private String forename;
	
	public Person(String name, String forename) {
		this.name = name;
		this.forename = forename;
	}
	
	public String getName() { return this.name; }
	public String getForename() { return this.forename; }
	
	public String toString() { return this.getName()+" "+this.getForename(); }
}
