package tp03_qd;

public class Student1 extends Person{
	
	public static final int DEFAULT_DURATION = 20;
	private int duration;
	
	Student1(String name, String forename, int duration) {
		super(name, forename);
		this.duration = duration;
	}
	
	Student1(String name, String forename){
		super(name, forename);
		this.duration = Student1.DEFAULT_DURATION;
	}
	
	public String toString() {
		return this.getName() + " " + this.getForename() + "(" + this.duration + ")";
	}
}
