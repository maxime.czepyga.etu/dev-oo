package tp03_qd;
import java.time.LocalDate;
import java.time.LocalTime;

public class Defense {

	private LocalDate whenDay;
	private Room where;
	private Student who;
	private LocalTime whenHour;
	private String title;
	
	public Defense(LocalDate whenDay, Room where, Student who, LocalTime whenHour, String title) {
		this.whenDay = whenDay;
		this.where = where;
		this.who = who;
		this.whenHour = whenHour;
		this.title = title;
	}
	
	public Defense(Room where, Student who, String title) {
		this.whenDay = LocalDate.now();
		this.where = where;
		this.who = who;
		this.whenHour = LocalTime.now();
		this.title = title;
	}
	
	public String toString() { return this.whenDay + ":" + this.whenHour+" in "+this.where+" -> "+this.who+" :: "+this.title; }
	
	public LocalDate getDay() { return this.whenDay; }
	public LocalTime getHour() { return this.whenHour; }
	public LocalTime getTitle() { return this.title; }
	public Room getPlace() { return this.where; }
}
