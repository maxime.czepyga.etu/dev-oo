package tp01;
import java.lang.Math;   

public class RandomSequence {
	
	public static void main(String [] args) {
		
		//Initialisation des variables
		int nb;
		int max;
		double rand;
		
		//Precondition
		if (args.length<2 || args.length>3) {
			System.out.println("Correct usage : <nbElt> <maxVal> [INTEGER|REAL]");
		} else {
			
			//Convertir un String en int
			nb = Integer.parseInt(args[0]);
			max = Integer.parseInt(args[1]);
			
			if (args.length==3 && args[2].length()==4) {
				for (int i=0; i<nb; i++) {
					rand = Math.random();
					System.out.print((max*rand) + ",");
				}
			} else {
				for (int i=0; i<nb; i++) {
					rand = Math.random();
					System.out.print((int)(max*rand) + ",");
				}
			}
		}
	}
}
