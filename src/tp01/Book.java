package tp01;

public class Book {

	// class attributes
	private String author;
	private String title;
	private int year;
	
	// constructor
	public Book(String author, String title, int year) {
		this.author = author;
		this.title = title;
		this.year = year;
	}
	// methods
	public String getAuthor() {
		return this.author;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public String toString() {
		return this.getAuthor()+" a �crit "+this.getTitle()+" en "+this.getYear();
	}
	
}
