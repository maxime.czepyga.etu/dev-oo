package tp01;

public class UseBook {
	 
	public static void main(String[] args) {
		Book[] biblio = new Book[] { new Book("Jules Verne", "20000 lieux sous les mers", 1870),
									 new Book("JK Rowling", "Harry Potter et la coupe de feu", 2000),
									 new Book("Veronica Roth", "Divergente", 2011) };
		for (int i=0; i<biblio.length; i++) {
			System.out.println(biblio[i]);
		}
	}
}
