package tp01;
import java.lang.Math;

public class Irregular {
	
	int[][] myTable;
	
	Irregular(int[] lineSize){
		this.myTable = new int[lineSize.length][];
		int[] a;
		for (int i=0; i<lineSize.length; i++) {
			a = new int[lineSize[i]];
			this.myTable[i] = a;
		}
	}
	
	void randomFilling() {
		for (int i=0; i<this.myTable.length; i++) {
			for (int j=0; j<this.myTable[i].length; j++) {
				this.myTable[i][j] = (int)(Math.random()*10);
			}
		}
	}
	
	boolean isCommun(int element) {
		for (int i=0; i<this.myTable.length; i++) {
			for (int j=0; j<this.myTable[i].length; j++) {
				if(this.myTable[i][j]==element) {
					return true;
				}
			}
		}
		return false;
	}
	
	boolean existCommon() {
		
		int number = 1;
		boolean isNumber = false;
		
		for (int x=0; x<this.myTable[0].length; x++) {
			for (int i=1; i<this.myTable.length; i++) {
				for (int j=0; j<this.myTable[i].length; j++) {
					if(this.myTable[i][j]==myTable[0][x]) {
						isNumber=true;
					}
				}
				if (isNumber) {
					number += 1;
				}
				isNumber = false;
			}
			if (number==this.myTable.length) {
				return true;
			} else {
				number = 1;
			}
		}
		return false;
	}
	
	String display() {
		String result = "";
		for (int i=0; i<this.myTable.length; i++) {
			for (int j=0; j<this.myTable[i].length; j++) {
				result += this.myTable[i][j];
			}
			result += "\n";
		}
		return result;
	}
}
